module git.dotya.ml/wanderer/ak0da

go 1.20

require (
	github.com/sjwhitworth/golearn v0.0.0-20221228163002-74ae077eafb2
	golang.org/x/exp v0.0.0-20230510235704-dd950f8aeaea
	gonum.org/v1/gonum v0.13.0
)

require (
	github.com/google/go-cmp v0.5.8 // indirect
	github.com/guptarohit/asciigraph v0.5.1 // indirect
	github.com/mattn/go-runewidth v0.0.7 // indirect
	github.com/olekukonko/tablewriter v0.0.4 // indirect
	github.com/rocketlaunchr/dataframe-go v0.0.0-20201007021539-67b046771f0b // indirect
	golang.org/x/sync v0.0.0-20200317015054-43a5402ce75a // indirect
)
