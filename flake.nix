{
  description = "Data Analysis";
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    nix-filter = {
      url = "github:numtide/nix-filter";
    };
  };

  outputs = {
    self,
    nixpkgs,
    nix-filter,
    ...
  }: let
    projname = "ak0da";

    # to work with older version of flakes
    lastModifiedDate =
      self.lastModifiedDate or self.lastModified or "19700101";

    # Generate a user-friendly version number.
    version = "v0.0.0";

    supportedSystems = ["x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin"];
    forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
    pkgs = forAllSystems (system: nixpkgs.legacyPackages.${system});
    # Nixpkgs instantiated for supported system types.
    nixpkgsFor = forAllSystems (system:
      import nixpkgs {
        inherit system;
        overlays = [
          # no overlay imports atm
          # (import ./overlay.nix)
        ];
      });
  in {
    formatter = forAllSystems (
      system:
        nixpkgsFor.${system}.alejandra
    );
    packages = forAllSystems (system: let
      baseurl = "https://git.dotya.ml/wanderer/ak0da/";
      pkgs = nixpkgsFor.${system};
      inherit (pkgs) lib overlays;
    in rec {
      ak0da = with pkgs;
        buildGo120Module rec {
          pname = "proj";
          buildInputs = [
            go_1_20
          ];
          nativeBuildInputs = [pkgconfig];

          overrideModAttrs = _: {
            # GOPROXY = "direct";
            GOFLAGS = "-buildmode=pie -trimpath -mod=readonly -modcacherw";
          };

          inherit version;
          doCheck = false;
          # use go.mod for managing go deps, instead of vendor-only dir
          proxyVendor = true;
          tags = []; # go "-tags" to build with
          ldflags = [
            "-s"
            "-w"
          ];

          # dont't forget to update vendorSha256 whenever go.mod or go.sum change
          vendorSha256 = "sha256-0rRm+JU5m22WnkCYxLZTEbrsUpA942A6P+clWDMUknA=";

          # In 'nix develop', we don't need a copy of the source tree
          # in the Nix store.
          src = nix-filter.lib.filter {
            # when in doubt, check out
            # https://github.com/numtide/nix-filter#design-notes
            # tl;dr: it'd be best to include folders, however there are
            # currently issues with that approach.
            root = lib.cleanSource ./.;
            exclude = [
              ./flake.nix
              ./flake.lock
              ./default.nix
              ./shell.nix
              ./overlay.nix

              ./README.md

              ./.envrc
              ./.gitattributes
              ./.gitignore
              ./.golangci.yml
              ./.editorconfig
              ./.pre-commit-config.yaml

              # program output
              ./out
              ./res

              # nix result symlink
              ./result

              # the entire .git folder
              ./.git
            ];
          };

          meta = {
            description = "implementation of the compulsory task for ak0da";
            homepage = baseurl;
            license = lib.licenses.gpl3;
            maintainers = ["wanderer"];
            platforms = lib.platforms.linux ++ lib.platforms.darwin;
          };
        };

      default = ak0da;
    });

    apps = forAllSystems (system: rec {
      proj = {
        type = "app";
        program = "${self.packages.${system}.${projname}}/bin/ak0da";
      };
      default = proj;
    });

    devShells = forAllSystems (system: let
      upcache = pkgs.writeShellScriptBin "upcache" ''
        ## refs:
        ##   https://fzakaria.com/2020/08/11/caching-your-nix-shell.html
        ##   https://nixos.wiki/wiki/Caching_nix_shell_build_inputs
        nix-store --query --references $(nix-instantiate shell.nix) | \
          xargs nix-store --realise | \
          xargs nix-store --query --requisites | \
          cachix push ${projname}
        nix build --json \
          | jq -r '.[].outputs | to_entries[].value' \
          | cachix push ${projname}
      '';
      add-license = pkgs.writeShellScriptBin "add-license" ''
        go run github.com/google/addlicense@v1.0.0 -v \
          -c "wanderer <a_mirre at utb dot cz>" \
          -l "GPL-3.0-or-later" -s .
      '';
    in {
      default = pkgs.${system}.mkShellNoCC {
        name = "${projname}-" + version;

        GOFLAGS = "-buildmode=pie -trimpath -mod=readonly -modcacherw";
        GOLDFLAGS = "-s -w";
        CGO_CFLAGS = "-g0 -Ofast -mtune=native -flto";
        CGO_LDFLAGS = "-Wl,-O1,-sort-common,-as-needed,-z,relro,-z,now,-flto -pthread";

        shellHook = ''
          echo " -- in ${projname} dev shell..."
        '';

        packages = with pkgs.${system}; [
          pre-commit
          statix

          # deps
          go_1_20
          go-tools
          gofumpt
        ];
      };
    });
  };
}
