# [ak0da](https://git.dotya.ml/wanderer/ak0da)

this repo contains code of the compulsory task for `AK0DA` course.

the topic is anomaly/outlier detection using
[`golearn`](https://github.com/sjwhitworth/golearn)'s Isolation Forest model
implementation.

to test the functionality you'd need:
* [Go](https://go.dev/) (v1.20.x+)
* alternatively, you could use [Nix](https://nixos.org)

with Go ready, do:
```sh
go install -v git.dotya.ml/wanderer/ak0da@latest
$GOPATH/bin/ak0da

# or to run the code directly:
go run -v git.dotya.ml/wanderer/ak0da@latest
```

or with (flake-enabled) Nix, do:
```sh
nix run git+https://git.dotya.ml/wanderer/ak0da#proj
```

both of the above should put the `ak0da` binary in your `$GOPATH`, from where
you can run it.<br>
it then drops a `data.csv` file in `$PWD` which is overwritten everytime the
program is run.

the last 15 (the amount is hardcoded) values are outliers, which should stand
out to the model and be detected as such.

by default there are 10001 *"normal"* (standard normal distribution or uniform
distribution `< -1; 1 >`) values generated, to array of which the outliers are
appended.

example output:
```
$ go run -v .
git.dotya.ml/wanderer/ak0da
2023/05/14 20:58:19 generating data
2023/05/14 20:58:19 generating data - done
2023/05/14 20:58:19 saving data at 'data.csv'
2023/05/14 20:58:19 writing data
2023/05/14 20:58:19 writing data - done
2023/05/14 20:58:19 Train Isolation Forest model (nTrees: 1000, maxDepth: 1000, subSpace: 7500) on the data
2023/05/14 20:58:41 Calculate avg/min scores
	Average anomaly score for normal data: 0.410835
	Minimum anomaly score for normal data: 0.358608
	Anomaly scores for outliers are:
	0.8326351331269884
	0.8418463721853892
	0.8770692966745977
	0.8252221454618904
	0.8778969291657617
	0.8484648511184422
	0.8599306977267279
	0.8585139586360264
	0.8494674322646776
	0.8588444630459295
	0.8507734185366144
	0.8796766012823853
	0.868373052855811
	0.852372446867535
	0.8598396082607581
2023/05/14 20:58:41 we're done

```
